package Server;

import android.content.res.Resources;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ArtistDTOService {
    public ArtistDTOService() {
    }

    public ArtistDTO getInfoFromRequest(String requestString){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(requestString, ArtistDTO.class);
        } catch (JsonProcessingException e) {
           throw new Resources.NotFoundException();
        }

    }
}
