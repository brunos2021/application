package Server;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.Picasso;

public class ServerRequest extends AppCompatActivity implements View.OnClickListener {

    private final Context context;
    TextView textView, textView1, textView2, textView3;
    ImageView imageView;
    EditText editText;
    String singer;


    String server_url = "http://10.0.2.2:8080/artists/api/";


    public ServerRequest(Context context, TextView textView, TextView textView1, TextView textView2,TextView textView3, ImageView imageView, EditText editText) {

        this.textView = textView;
        this.textView1 = textView1;
        this.textView2 = textView2;
        this.textView3 = textView3;
        this.imageView = imageView;
        this.context = context;
        this.editText = editText;

    }


    private ArtistDTOService artistDTOService = new ArtistDTOService();

    @Override
    public void onClick(View v) {

        this.singer = editText.getText().toString();

        RequestQueue requestQueue = Volley.newRequestQueue(this.context);

                ObjectMapper objectMapper = new ObjectMapper();
                StringRequest stringRequest = new StringRequest(Request.Method.GET, server_url+singer,

                        response -> {


                            ArtistDTO artistDTO = artistDTOService.getInfoFromRequest(response);

                            textView1.setMovementMethod(new ScrollingMovementMethod());
                            textView.setText(artistDTO.getArtistname());
                            textView1.setText(artistDTO.getBio());
                            textView2.setText(artistDTO.getFacebook());
                            textView3.setText(artistDTO.getTwitter());
                            Picasso.get().load(artistDTO.getImage()).into(imageView);

                            requestQueue.stop();


                        }
                        , error -> {
                            textView.setText("error");
                            error.printStackTrace();

                            requestQueue.stop();

                        });


                requestQueue.add(stringRequest);


    }
}