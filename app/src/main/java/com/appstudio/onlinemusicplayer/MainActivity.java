package com.appstudio.onlinemusicplayer;

import Server.ServerRequest;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.view.JcPlayerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    Button button;
    TextView textView,textView1,textView2,textView3;
    ImageView imageView;
    EditText editText;
    String singer;

    String server_url = "http://172.18.208.1:8080/";


    ArrayAdapter<String> arrayAdapter;



    private JcPlayerView jcplayerView;


      @Override
      protected void onCreate(Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);
          button = (Button)findViewById(R.id.bn);
          textView = (TextView)findViewById(R.id.txt);
          textView1 = (TextView)findViewById(R.id.txt1);
          textView2 = (TextView)findViewById(R.id.txt2);
          textView3 = (TextView)findViewById(R.id.txt3);
          imageView = (ImageView)findViewById(R.id.image);
          editText  = (EditText)findViewById(R.id.edit_txt);
          button.setOnClickListener(new ServerRequest(this.getApplicationContext(), textView, textView1, textView2, textView3, imageView, editText));



      //  ListView listView = findViewById(R.id.list);
        List<String> list = new ArrayList<>();
        list.add("1.song"); list.add("2.song");
        list.add("3.song"); list.add("4.song");
        list.add("5.song"); list.add("6.song");

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        //listView.setAdapter(arrayAdapter);


        String url1="https://firebasestorage.googleapis.com/v0/b/online-music-player-3959d.appspot.com/o/Elvis%20Presley%20-%20Can't%20Help%20Falling%20In%20Love%20(Official%20Audio).mp3?alt=media&token=f4611a8c-fe1d-4c46-bd80-594d4ffdf425";
        String url2="https://firebasestorage.googleapis.com/v0/b/online-music-player-3959d.appspot.com/o/Eminem%20-%20Mockingbird%20(Official%20Music%20Video).mp3?alt=media&token=c0ddc076-164d-4868-8749-140f4dfc83bf";
        String url3="https://firebasestorage.googleapis.com/v0/b/online-music-player-3959d.appspot.com/o/Fly%20Me%20To%20The%20Moon%20(2008%20Remastered).mp3?alt=media&token=9f8cd404-9a54-4228-bfd8-00375d9e9519";
        String url4="https://firebasestorage.googleapis.com/v0/b/online-music-player-3959d.appspot.com/o/Mad%20Clip%20%26%20Eleni%20Foureira%20-%20Mporei%20-%20Official%20Music%20Video.mp3?alt=media&token=d71c54ee-1967-4306-b28c-2a142e82ac40";
        String url5="https://firebasestorage.googleapis.com/v0/b/online-music-player-3959d.appspot.com/o/Soldier%20Of%20Fortune%20-%20Deep%20Purple%20(Lyric).mp3?alt=media&token=acc3f99b-b603-4675-9974-974fdc740ea3";

        JcPlayerView JcPlayerView=findViewById(R.id.jcplayer);



        ArrayList<JcAudio> jcAudios = new ArrayList<>();
        jcAudios.add(JcAudio.createFromURL("Elvis Presley - Can't Help Falling In Love",url1));
        jcAudios.add(JcAudio.createFromURL("Eminem - Mockingbird",url2));
        jcAudios.add(JcAudio.createFromURL("Frank Sinatra - Fly Me To The Moon (2008 Remastered)",url3));
        jcAudios.add(JcAudio.createFromURL("Mad Clip & Eleni Foureira - Mporei",url4));
        jcAudios.add(JcAudio.createFromURL("Soldier Of Fortune - Deep Purple",url5));

        JcPlayerView.initPlaylist(jcAudios, null);

        JcPlayerView.createNotification(); // default icon


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Search Here!");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                arrayAdapter.getFilter().filter(s);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}

